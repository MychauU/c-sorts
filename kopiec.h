#define kop
#include <time.h>
#include <iostream>
#define RAND 1000
using namespace std;
struct heap{
	int *tab;
	int size;
	int size_active;
};
//zwraca lewe dziecko 
int heap_R_child(int &i);
//zwraca prawe dziecko
int heap_L_child(int &i);
//zwraca rodzica
int heap_parent(int &v);
//zwraca poprawnosc kopca dla pojedynczego poddrzewa
void heapify(heap &kopiec,int &i,int &size);
//zwraca poprawnosc kopca dla wszystkich poddrzew
void heap_build(heap &kopiec);
//zwraca posortowany kopiec
void heap_sort(heap &kopiec);
//zwraca tablice nieposartowana z nowymi random elementami
void heap_randomize(heap &kopiec);
// wyswietla kopiec
void heap_display(heap &kopiec);
//dodaje element do kopca
void heap_insert(heap &kopiec, int x);
int heap_remove(heap &kopiec);
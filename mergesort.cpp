#include "mergesort.h"
int t[100000];
void merge(int tab[],int &pocz, int &sr, int &kon){

int i,j,q;
//int *t = new int[stale];
for (i=pocz; i<=kon; i++) t[i]=tab[i];  // Skopiowanie danych do tablicy pomocniczej
i=pocz; j=sr+1; q=pocz;                 // Ustawienie wska�nik�w tablic
while (i<=sr && j<=kon) {         // Przenoszenie danych z sortowaniem ze zbior�w pomocniczych do tablicy g��wnej
if (t[i]<t[j])
tab[q++]=t[i++];
else
tab[q++]=t[j++];
}
while (i<=sr) tab[q++]=t[i++]; // Przeniesienie nie skopiowanych danych ze zbioru pierwszego w przypadku, gdy drugi zbi�r si� sko�czy�
//delete[] t;
}
 
/* Procedura sortowania tab[pocz...kon] */
void merge_sort(int tab[],int pocz, int kon){
int sr;
if (pocz<kon) {
	sr=(pocz+kon)/2;
	merge_sort(tab,pocz, sr);    // Dzielenie lewej cz�ci
	merge_sort(tab,sr+1, kon);   // Dzielenie prawej cz�ci
	merge(tab, pocz, sr, kon);   // ��czenie cz�ci lewej i prawej
}
}

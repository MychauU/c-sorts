#include "quicksort.h"
void quick_sort(int tab[], int first, int last){
	int q;
	if (first < last){
		q = quick_partition(tab, first, last);
		quick_sort(tab, first, q);
		quick_sort(tab, q+1, last);
	}
}
int quick_partition(int tab[],int &first,int &last){
	int	x = tab[(first+last)>>1];
	int i = first - 1;
	int j = last + 1;
	int pom;
	while (true){
		do{
			j = j - 1;
		} while (tab[j] > x);
		do{
			i = i + 1;
				
		} while (tab[i] < x);
		if (i < j) {
			pom = tab[i];
			tab[i] = tab[j];
			tab[j] = pom;
		}
		else  return j;
	}
}
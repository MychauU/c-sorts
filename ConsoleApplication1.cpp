#include <time.h>
#include <iostream>
using namespace std;
#define SIZE 10
#include "kopiec.h"
#include "quicksort.h"
#include "simplesort.h"
#include "shellsort.h"
#include "mergesort.h"
int main(void) {
	srand(time(0));
	heap kopiec;
	kopiec.tab = new int[SIZE]{};
	kopiec.size = SIZE;
	kopiec.size_active=0;
	/* Do your stuff here */
	int m = SIZE;
	int tab[SIZE] = {};
	heap_randomize(kopiec);
//	array_randomize(tab,m);
//	array_display(tab, m);
	clock_t tStart = clock();
//	heap_remove(kopiec);
//	heap_insert(kopiec, 20);
	heap_display(kopiec);
//	quick_sort(tab, 0, m-1);
//	merge_sort(tab, 0, m-1);
//  array_display(tab, m);
	printf("Time taken: %.2fs\n", (double)(clock() - tStart) / CLOCKS_PER_SEC);
	getchar();
	return 0;
}
#include "kopiec.h"
int heap_L_child(int &i){
	return 2*i;
}
int heap_R_child(int &i){
	return 2 * i + 1;
}
int heap_parent (int &v)
{
	return v / 2;
}
void heapify(heap &kopiec, int &i,int &size){
	int maxps;
	int L = heap_L_child(i);
	int P = heap_R_child(i);
	if (L <= size && kopiec.tab[L - 1] <= kopiec.tab[i - 1]) maxps = L;
	else maxps = i;
	if (P <= size  && kopiec.tab[P - 1] < kopiec.tab[maxps - 1]) maxps = P;
	if (maxps != i){
		int pom = kopiec.tab[i - 1];
		kopiec.tab[i - 1] = kopiec.tab[maxps - 1];
		kopiec.tab[maxps - 1] = pom;
			heapify(kopiec, maxps, size);
	}
}
void heap_build(heap &kopiec){
	for (int i = kopiec.size_active / 2; i >0; i--){
		heapify(kopiec, i, kopiec.size_active);
	}
}
void heap_sort(heap &kopiec){
	heap_build(kopiec);
	int pom;
	int l = 1;
	for (int i = kopiec.size-1; i >= 1; i--){
		pom = kopiec.tab[i];
		kopiec.tab[i] = kopiec.tab[0];
		kopiec.tab[0] = pom;
		heapify(kopiec, l, i);
	}
}
void heap_insert(heap &kopiec,int x){
	if (kopiec.size_active == kopiec.size);
	else {
		kopiec.tab[kopiec.size_active] = x; //wstaw na koniec
		kopiec.size_active++;           //zwieksz liczbe elementow kopca
		heap_build(kopiec);
	}
}
int heap_remove(heap &kopiec){
	int x;
	if (kopiec.size_active == 0)
		return 0;
	kopiec.tab[0] = kopiec.tab[--kopiec.size_active];
	heap_build(kopiec);
	x = kopiec.tab[kopiec.size_active];
	kopiec.tab[kopiec.size_active] = 0;
	return x;
}
void heap_randomize(heap &kopiec){
	srand(time(NULL));
	for (int i = 0; i < kopiec.size; ++i){
		kopiec.tab[i] = rand() % RAND + 1;
	}
	kopiec.size_active = kopiec.size;
	heap_build(kopiec);

}

void heap_display(heap &kopiec){
	for (int i = 0; i < kopiec.size_active; ++i){
		cout << kopiec.tab[i] << " ";
	}
	cout << endl;

}
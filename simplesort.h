#ifndef simplesort
#define simplesort
#endif
#include <time.h>
#include <iostream>
#define RAND 1000
using namespace std;

void bubble_sort(int tab[], const int m);
void insertion_sort(int tab[], int m);
void array_display(int tab[], int m);
void array_randomize(int tab[], int m);
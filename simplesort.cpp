#include "simplesort.h"
void bubble_sort(int tab[], const int m){
	bool change;
	int pom;
	for (int j = m - 1; j > 0; --j){
		change = false;
		for (int i = 0; i <= j; ++i){
			if (tab[i + 1] > tab[i]){
				pom = tab[i + 1];
				tab[i + 1] = tab[i];
				tab[i] = pom;
				change = true;
			}
		}
		if (change == false) break;
	}
}
void insertion_sort(int tab[], int m){
	int x;
	int j;
	for (int i = 1; i < m; i++){
		x = tab[i];
		for (j = i - 1; j >= 0; --j){
			if (x >= tab[j]) break;
			tab[j + 1] = tab[j];
		}
		tab[j + 1] = x;
	}
}
void array_randomize(int tab[], int m){
	srand(time(NULL));
	for (int i = 0; i < m; ++i){
		tab[i] = rand() % RAND + 1;
	}


}
void array_display(int tab[], int m){
	for (int i = 0; i < m; ++i){
		cout << tab[i] << " ";
	}
	cout << endl;
}

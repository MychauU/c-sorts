#include "shellsort.h"
#include <math.h>
void shell_sort(int tab[], int size){
	int k = pow(2, log((double)size)) - 1;
	int x;
	int j;
	while (k >= 1){
		for (int i = k; i < size; i++){
			x = tab[i];
			j = i - k;
			while (j >= 0 && x < tab[j]){
				tab[j + k] = tab[j];
				j -= k;
			}
			tab[j + k] = x;
		}
		k = (k + 1) / 2 - 1;
	}
}